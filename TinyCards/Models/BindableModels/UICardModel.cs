﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using Prism.Commands;
using PropertyChanged;

namespace TinyCards.Models.BindableModels
{
    [AddINotifyPropertyChangedInterface]
    public class UICardModel : CardModel
    {
        public bool IsShowDescription { get; set; } = true;

        private ICommand _flipCardCommand;
        public ICommand FlipCardCommand => _flipCardCommand ?? (_flipCardCommand = new DelegateCommand(() => IsShowDescription = !IsShowDescription));
    }
}
