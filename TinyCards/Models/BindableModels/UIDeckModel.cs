﻿using System;
using System.Windows.Input;
using PropertyChanged;

namespace TinyCards.Models.BindableModels
{
    [AddINotifyPropertyChangedInterface]
    public class UIDeckModel : DeckModel
    {
        public ICommand ActionCommand { get; set; }
    }
}
