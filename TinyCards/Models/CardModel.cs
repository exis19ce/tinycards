﻿using System;
using SQLite;

namespace TinyCards.Models
{
    public class CardModel : ISqlModel
    {
        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }
        public string Word { get; set; }
        public string FirstDescription { get; set; }
        public string SecondDescription { get; set; }
    }
}
