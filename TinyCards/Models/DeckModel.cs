﻿using System.Collections.Generic;
using SQLite;

namespace TinyCards.Models
{
    public class DeckModel : ISqlModel
    {
        [PrimaryKey]
        [AutoIncrement]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public List<CardModel> CardsIds { get; set; }
    }
}
