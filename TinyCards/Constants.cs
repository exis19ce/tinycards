﻿using System;
using System.IO;

namespace TinyCards
{
    public static class Constants
    {
        public const string DB_NAME = "TinyCardsDb.db3";
        //public static readonly string DB_PATH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), DB_NAME);
        public const int DB_VERSION = 1;
    }
}
