﻿using System;
using AutoMapper;
using Microsoft.Extensions.Configuration;
using TinyCards.Models;
using TinyCards.Models.BindableModels;

namespace TinyCards.Helpers
{
    public class AutoMaper : IAutoMaper
    {
        private IMapper _mapper;
        private object lockObject = new object();
        private bool _isConfigured;

        public void Configure()
        {
            lock (lockObject)
            {
                if (!_isConfigured)
                {
                    ConfigureUserMapping();
                    _isConfigured = true;
                }
            }
        }

        private void ConfigureUserMapping()
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.ShouldMapProperty = p => p.GetMethod.IsPublic || p.GetMethod.IsVirtual;
                cfg.CreateMap<UICardModel, CardModel>();
                cfg.CreateMap<CardModel, UICardModel>();
                cfg.CreateMap<UIDeckModel, DeckModel>();
                cfg.CreateMap<DeckModel, UIDeckModel>();
            });

            _mapper = configuration.CreateMapper();
        }

        public TDestination Map<TDestination>(object source)
        {
            if (!_isConfigured)
                Configure();

            return _mapper.Map<TDestination>(source);
        }
    }
}