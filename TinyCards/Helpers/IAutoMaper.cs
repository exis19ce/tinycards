﻿using System;
namespace TinyCards.Helpers
{
    public interface IAutoMaper
    {
        void Configure();
        TDestination Map<TDestination>(object source);
    }
}
