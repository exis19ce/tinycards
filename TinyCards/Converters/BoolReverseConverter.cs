﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace TinyCards.Converters
{
    public class BoolReverseConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return GetReverseValue(value);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return GetReverseValue(value);
        }

        private bool GetReverseValue(object value)
        {
            if (value is bool boolVal)
                return !boolVal;

            return true;
        }
    }
}
