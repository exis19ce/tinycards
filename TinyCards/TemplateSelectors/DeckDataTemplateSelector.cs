﻿using System;
using TinyCards.Controls;
using TinyCards.Models;
using Xamarin.Forms;
using Xamarin.Forms.PancakeView;

namespace TinyCards.TemplateSelectors
{
    public class DeckDataTemplateSelector : DataTemplateSelector
    {
        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {
            //if (item is DeckModel deck && deck.Id == -1)
            //    return new DataTemplate(() =>
            //    {
            //        return new PancakeView
            //        {
            //            Margin = 10,
            //            HeightRequest = 60,
            //            WidthRequest = 60,
            //            Padding = 10,
            //            CornerRadius = new CornerRadius(40),
            //            BorderColor = Color.FromHex("#bc91d7"),
            //            BorderDrawingStyle = BorderDrawingStyle.Inside,
            //            BorderThickness = 2,
            //            Content = new Label()
            //            {
            //                HorizontalOptions = LayoutOptions.CenterAndExpand,
            //                VerticalOptions = LayoutOptions.CenterAndExpand,
            //                FontSize = 30,
            //                TextColor = Color.FromHex("#bc91d7"),
            //                Text = "+"
            //            }
            //        };
            //    });

            return new DataTemplate(typeof(CardControl));
        }
    }
}
