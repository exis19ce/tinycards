﻿using Prism;
using Prism.Ioc;
using Prism.Plugin.Popups;
using TinyCards.Helpers;
using TinyCards.Services.DataBase;
using TinyCards.ViewModels;
using TinyCards.Views;
using Xamarin.Forms;

namespace TinyCards
{
    public partial class App
    {
        public App()
        {
        }

        public App(IPlatformInitializer initializer = null) : base(initializer)
        {
            InitializeComponent();

#if DEBUG
            EnableDebugRainbows(false);
#endif
        }

        protected override void OnInitialized()
        {
            MainPage = new ContentPage();
            NavigationService.NavigateAsync("/" + nameof(NavigationPage ) + "/" + nameof(MainView));
        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            containerRegistry.RegisterPopupNavigationService();
            containerRegistry.RegisterInstance<IDataBaseContext>(Container.Resolve<DataBaseContext>());

            var maper = Container.Resolve<AutoMaper>();
            maper.Configure();
            containerRegistry.RegisterInstance<IAutoMaper>(maper);

            containerRegistry.RegisterForNavigation<NavigationPage>();
            containerRegistry.RegisterForNavigation<MainView>();
            containerRegistry.RegisterForNavigation<CardView>();
            containerRegistry.RegisterForNavigation<DeckView>();
        }

        #region --LifeCicle--

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        #endregion

        private void EnableDebugRainbows(bool shouldUseDebugRainbows)
        {
            Resources.Add(new Style(typeof(ContentPage))
            {
                ApplyToDerivedTypes = true,
                Setters = {
                new Setter
                {
                    Property = Xamarin.Forms.DebugRainbows.DebugRainbow.IsDebugProperty,
                    Value = shouldUseDebugRainbows
                }
            }});

            Resources.Add(new Style(typeof(ContentView))
            {
                ApplyToDerivedTypes = true,
                Setters = {
                new Setter
                {
                    Property = Xamarin.Forms.DebugRainbows.DebugRainbow.IsDebugProperty,
                    Value = shouldUseDebugRainbows
                }
            }
            });
        }
    }
}
