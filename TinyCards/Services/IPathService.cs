﻿using System;
namespace TinyCards.Services
{
    public interface IPathService
    {
        string GetDatabasePath(string filename);
    }
}
