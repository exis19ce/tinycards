﻿using System;
namespace TinyCards.Services.DataBase
{
    public interface IBaseDbContext
    {
        void SaveChanges();
    }
}
