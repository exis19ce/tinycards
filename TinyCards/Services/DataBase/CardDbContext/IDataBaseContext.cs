﻿using System;
using Microsoft.EntityFrameworkCore;
using TinyCards.Models;

namespace TinyCards.Services.DataBase
{
    public interface IDataBaseContext : IBaseDbContext
    {
        DbSet<CardModel> Cards { get; set; }
        DbSet<DeckModel> Decks { get; set; }
    }
}
