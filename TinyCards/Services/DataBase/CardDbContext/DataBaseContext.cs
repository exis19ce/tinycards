﻿using System;
using Microsoft.EntityFrameworkCore;
using TinyCards.Models;

namespace TinyCards.Services.DataBase
{
    public class DataBaseContext : BaseDbContext, IDataBaseContext
    {
        public DbSet<CardModel> Cards { get; set; }
        public DbSet<DeckModel> Decks { get; set; }

        public DataBaseContext(IPathService pathService) : base(pathService)
        {
        }
    }
}
