﻿using System;
using Microsoft.EntityFrameworkCore;

namespace TinyCards.Services.DataBase
{
    public class BaseDbContext : DbContext
    {
        private readonly string _databasePath;

        public BaseDbContext(IPathService pathService)
        {
            _databasePath = pathService.GetDatabasePath(Constants.DB_NAME);
            this.Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite($"Filename={_databasePath}");
            optionsBuilder.EnableSensitiveDataLogging();
        }

        public new void SaveChanges() => base.SaveChanges();
    }
}
