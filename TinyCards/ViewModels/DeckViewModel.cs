﻿using System.Drawing;
using AutoMapper;
using Prism.Commands;
using Prism.Navigation;
using TinyCards.Helpers;
using TinyCards.Models;
using TinyCards.Models.BindableModels;
using TinyCards.Services.DataBase;

namespace TinyCards.ViewModels
{
    public class DeckViewModel : BaseViewModel, IInitialize
    {
        private readonly IDataBaseContext _dbContext;
        private readonly INavigationService _navigationService;
        private readonly IAutoMaper _autoMaper;

        private DelegateCommand _submitCommand;

        public DeckViewModel(IDataBaseContext dbContext,
            INavigationService navigationService,
            IAutoMaper autoMaper)
        {
            _autoMaper = autoMaper;
            _dbContext = dbContext;
            _navigationService = navigationService;
        }

        #region -- Properties --

        public bool IsEditExistDeck { get; set; }

        public Color PageBackGroundColor => Color.FromArgb(92, Color.Black);

        public UIDeckModel Deck { get; set; }

        public DelegateCommand SubmitCommand => _submitCommand ?? (_submitCommand = new DelegateCommand(OnSubmitCommandExecuted));

        #endregion

        #region -- Private helpers --

        private async void OnSubmitCommandExecuted()
        {
            if (!string.IsNullOrEmpty(Deck.Title))
            {
                if (IsEditExistDeck)
                {
                    _dbContext.Decks.Update(_autoMaper.Map<DeckModel>(Deck));
                }
                else
                {
                    await _dbContext.Decks.AddAsync(Deck);
                }

                _dbContext.SaveChanges();
                await _navigationService.GoBackAsync();
            }
        }

        #endregion

        #region -- IInitialize implementation --

        public void Initialize(INavigationParameters parameters)
        {
            if (parameters.TryGetValue(nameof(UIDeckModel), out UIDeckModel deckModel))
            {
                Deck = deckModel;
                IsEditExistDeck = true;
            }
            else
            {
                Deck = new UIDeckModel();
            }
        }

        #endregion
    }
}
