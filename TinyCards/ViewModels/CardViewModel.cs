﻿using Prism.Commands;
using Prism.Navigation;
using TinyCards.Helpers;
using TinyCards.Models;
using TinyCards.Models.BindableModels;
using TinyCards.Services.DataBase;

namespace TinyCards.ViewModels
{
    public class CardViewModel : BaseViewModel, IInitialize
    {
        private readonly IDataBaseContext _dbContext;
        private readonly INavigationService _navigationService;
        private readonly IAutoMaper _autoMaper;

        private DelegateCommand _submitCommand;
        private DelegateCommand _removeCommand;

        private bool IsCardValid => !string.IsNullOrWhiteSpace(CardModel?.Word) && !string.IsNullOrWhiteSpace(CardModel?.FirstDescription);

        public CardViewModel(INavigationService navigationService,
            IDataBaseContext dbContext,
            IAutoMaper autoMaper)
        {
            _autoMaper = autoMaper;
            _dbContext = dbContext;
            _navigationService = navigationService;
        }

        #region --Public properties--

        public bool IsEditExistCard { get; set; }

        public UICardModel CardModel { get; set; }

        public DelegateCommand SubmitCommand => _submitCommand ??= new DelegateCommand(OnSubmitCommandExecuted);
        public DelegateCommand RemoveCommand => _removeCommand ??= new DelegateCommand(OnRemoveCommandExecuted);

        #endregion

        #region --Private helpers--

        private void OnRemoveCommandExecuted()
        {
            _dbContext.Cards.Remove(CardModel);
            _dbContext.SaveChanges();
            _navigationService.GoBackAsync();
        }

        private void OnSubmitCommandExecuted()
        {
            if (IsCardValid)
            {
                if (IsEditExistCard)
                {
                     _dbContext.Cards.Update(_autoMaper.Map<CardModel>(CardModel));
                }
                else
                {
                    _dbContext.Cards.Add(_autoMaper.Map<CardModel>(CardModel));
                }
                _dbContext.SaveChanges();
                _navigationService.GoBackAsync();
            }
        }

        #endregion

        #region -- IInitialize implementation --

        public void Initialize(INavigationParameters parameters)
        {
            if (parameters.TryGetValue(nameof(UICardModel), out UICardModel cardModel))
            {
                CardModel = cardModel;
                IsEditExistCard = true;
            }
            else
            {
                CardModel = new UICardModel();
            }
        }

        #endregion
    }
}
