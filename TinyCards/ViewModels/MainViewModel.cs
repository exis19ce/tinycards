﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;
using Prism.Commands;
using Prism.Navigation;
using TinyCards.Helpers;
using TinyCards.Models.BindableModels;
using TinyCards.Services.DataBase;
using TinyCards.Views;

namespace TinyCards.ViewModels
{
    public class MainViewModel : BaseViewModel, INavigationAware
    {
        private readonly INavigationService _navigationService;
        private readonly IAutoMaper _autoMaper;
        private readonly IDataBaseContext _cardDbContext;

        public ICommand _addNewDeckCommand;
        public ICommand _addNewCardCommand;
        public ICommand _selectDeckCommand;
        public ICommand _infoCommand;
        
        public MainViewModel(INavigationService navigationService, IDataBaseContext cardDbContext, IAutoMaper autoMaper)
        {
            _navigationService = navigationService;
            _cardDbContext = cardDbContext;
            _autoMaper = autoMaper;
        }

        #region --Public properties--

        public List<UICardModel> CardsList { get; set; }

        public List<UIDeckModel> DeckList { get; set; }
        
        public ICommand AddNewDeckCommand => _addNewDeckCommand ??= new DelegateCommand(OnAddNewDeck);
        public ICommand AddNewCardCommand => _addNewCardCommand ??= new DelegateCommand(OnAddNewCard);
        public ICommand SelectDeckCommand => _selectDeckCommand ??= new DelegateCommand<UIDeckModel>(OnSelectDeck);
        public ICommand InfoCommand => _infoCommand ??= new DelegateCommand<UICardModel>(OnCardInfoCommandAsync);

        #endregion

        #region --Private helpers--

        private async void OnCardInfoCommandAsync(UICardModel cardModel)
        {
            var param = new NavigationParameters
            {
                { nameof(UICardModel), cardModel }
            };

            await _navigationService.NavigateAsync(nameof(CardView), param);
        }

        private void OnAddNewDeck()
        {
            _navigationService.NavigateAsync(nameof(DeckView));
        }

        private void OnAddNewCard()
        {
            _navigationService.NavigateAsync(nameof(CardView));
        }

        private async void OnSelectDeck(UIDeckModel deckModel)
        {
            var param = new NavigationParameters
            {
                { nameof(UIDeckModel), deckModel }
            };

            await _navigationService.NavigateAsync(nameof(DeckView), param);
        }

        #endregion

        #region -- Overrides --

        public void OnNavigatedFrom(INavigationParameters parameters)
        {
        }

        public void OnNavigatedTo(INavigationParameters parameters)
        {
            var cardsList = _cardDbContext.Cards.ToList();
            var deckList = _cardDbContext.Decks.ToList();

            DeckList = _autoMaper.Map<List<UIDeckModel>>(deckList);
            CardsList = _autoMaper.Map<List<UICardModel>>(cardsList);
        }

        #endregion
    }
}
