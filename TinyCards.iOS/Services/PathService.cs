﻿using System;
using TinyCards.Services;
using System.IO;
using Xamarin.Forms;
using TinyCards.iOS.Services;

[assembly: Dependency(typeof(PathService))]
namespace TinyCards.iOS.Services
{
    public class PathService : IPathService
    {
        public string GetDatabasePath(string filename)
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "..", "Library", filename);
        }
    }
}
