﻿using System;
using System.IO;
using TinyCards.Services;

namespace TinyCards.Droid.Services
{
    public class PathService : IPathService
    {
        public string GetDatabasePath(string filename)
        {
            return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Personal), filename);
        }
    }
}
